## https://python-packaging.readthedocs.io/en/latest/
from setuptools import setup

setup(name='jshelpers',
      version='0.1',
      description='Collected functions',
      url='https://github.com/joergsteinkamp/jshelpers',
      author='Joerg Steinkamp',
      author_email='joerg.steinkamp@senckenberg.de',
      license='GPLv3',
      packages=['jshelpers'],
      scripts=['bin/shppreview'],
      install_requires=[
          'numpy',
          'xarray',
          'pycrs',
          'shapely',
          'geopandas',
          'rasterio'
      ],
      zip_safe=False)
