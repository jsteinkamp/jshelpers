def transform_from_latlon(lat, lon):
    """
    Stolen from https://github.com/pydata/xarray/issues/501
    """
    lat = np.asarray(lat)
    lon = np.asarray(lon)
    trans = Affine.translation(lon[0], lat[0])
    scale = Affine.scale(lon[1] - lon[0], lat[1] - lat[0])
    return trans * scale

def rasterizeGPD(shp, coords):
    """
    Rasterize a geopandas polygon dataframe
    """
    ## TODO: check coords names, so also x, long, longitude, y and lat are possible
    transform = transform_from_latlon(coords['lat'], coords['lon'])
    out_shape = (len(coords['lat']), len(coords['lon']))
    shapes = [(shape, n) for n, shape in enumerate(shp.geometry)]
    rmask = features.rasterize(shapes, fill=np.nan, out_shape=out_shape, transform=transform)
    xrmask = xr.DataArray(rmask, coords=coords, dims=('lat', 'lon'))
    return(xrmask)
