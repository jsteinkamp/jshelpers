import math
import numpy as np
import xarray as xr
from affine import Affine
from rasterio import features

def getBNDS(x):
    """
    Calculates the centers, left and right bounds between 1D numpy.ndarray elements
    """
    ## TODO: check, that x is a numpy array
    dx = np.diff(x)
    x_bnds = np.array([dx[0] / 2.0] + list(dx / 2.0) + [dx[x.size - 2] / 2.0])
    x_bnds[0:x.size] = x - x_bnds[0:x.size]
    x_bnds[-1] = x[-1]  + x_bnds[x.size]
    return(x_bnds)

def gridarea(a=None, lat=None, lon=None, wgs84=True):
    """
    Take an xarray.Dataset with lon/lat as dimensions or lat/lon as numpy.ndarray and calculates the area in each gridcell in m^2
    """

    if type(a) is xr.core.dataarray.DataArray:
        lat = a['lat'].values
        lon = a['lon'].values   
        area = xr.zeros_like(a)
    elif (type(lon) is np.ndarray) and (type(lat) is np.ndarray):
        area = xr.DataArray(np.zeros([lat.size, lon.size]),
                            coords={'lat':lat, 'lon':lon},
                            dims=['lat', 'lon'])
    else:
        raise ValueError('Either a as xarray.Dataset or lat/lon as npndarray must be given!')

    EarthRadius = np.zeros(lat.size) + 6371220.0
    if (wgs84):
        EarthRadius_polar   = 6356752.3142
        EarthRadius_equator = 6378137.0
        EarthRadius = EarthRadius_equator * np.cos(lat / 180.0 * math.pi) ** 2 + EarthRadius_polar   * np.sin(lat / 180.0 * math.pi) ** 2
    lat_bnds = getBNDS(lat)
    if (lon.size == 1):
        dlon=lon
    else:
        lon_bnds = getBNDS(lon)
        dlon = np.diff(lon_bnds)
    x = np.outer(np.cos(lat / 180.0 * math.pi) * 2.0 * math.pi * EarthRadius,  1.0 / (360.0 / dlon))
    y = 2 * math.pi * EarthRadius * (np.abs(lat_bnds[1:(lat.size + 1)] - lat_bnds[0:lat.size]) / 360.);
    ## y = np.repeat(y[:, np.newaxis], dlon.size, axis=1)
    v =  x * y[:, None]
    
    area[:] = v
    return area

## Example code for the gridarea function
## import numpy as np
## from jshelpers import gridarea
## lat = np.linspace(89.75, -89.75, 360)
## a slice of latitudes with width 0.5
## area = gridarea(lat=lat, lon=np.array([0.5]))
## print(area.sum() * 720.0e-12)
##
## whole globe with lat/lon
## area = gridarea(lat=lat, lon=lon)
## lon = np.linspace(-179.75, 179.75, 720)
## area = gridarea(lat=lat, lon=lon)
## print(area.sum()*1.e-12)
##
## import xarray as xr
## empty = xr.DataArray(np.empty([360, 720]), coords={'lat':lat, 'lon':lon}, dims=['lat', 'lon'])
## area  = gridarea(empty)
## area.sum() * 1.e-12
## all should be ~510

def transform_from_latlon(lat, lon):
    """
    Stolen from https://github.com/pydata/xarray/issues/501
    """
    lat = np.asarray(lat)
    lon = np.asarray(lon)
    trans = Affine.translation(lon[0], lat[0])
    scale = Affine.scale(lon[1] - lon[0], lat[1] - lat[0])
    return trans * scale

def rasterizeGPD(shp, coords):
    transform = transform_from_latlon(coords['lat'], coords['lon'])
    out_shape = (len(coords['lat']), len(coords['lon']))
    shapes = [(shape, n) for n, shape in enumerate(shp.geometry)]
    rmask = features.rasterize(shapes, fill=np.nan, out_shape=out_shape, transform=transform)
    xrmask = xr.DataArray(rmask, coords=coords, dims=('lat', 'lon'))
    return(xrmask)
