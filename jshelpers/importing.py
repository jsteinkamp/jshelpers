import os
import shapefile
import pycrs
import re
import warnings
from shapely.geometry import shape
from urllib2 import urlopen
from StringIO import StringIO
from zipfile import ZipFile
import geopandas as gpd

def shp2gpd(url):
    """Convert shapefile from any source to a geopandas dataframe
    
    Slightly modified from http://andrewgaidus.com/Reading_Zipped_Shapefiles/
    potential alternative: http://notes.ramondario.com/extracting-shapefiles-from-a-zip-virtual-file-server-in-fiona.html

    ## long if tree with
    ##   - real WWW URL, assuming zipped shp bundle
    ##   - local zipped shp bundle
    ## - full path to the .shp file
    ## - path to unzipped shapefile bundle. If two or more .shp files are present an error will occur
    ## - full path to the .shp file, but without .shp extention. This is usefull, if several .shp files are in one directory
    """
    if re.match("http|ftp", url) or re.search(r'.zip$', url):
        if re.match("http|ftp", url):
            zipfile = ZipFile(StringIO(urlopen(url).read()))
        else:
            zipfile = ZipFile(StringIO(open(url).read()))
        filenames = [y for y in sorted(zipfile.namelist()) for ending in ['dbf', 'prj', 'shp', 'shx'] if y.endswith(ending)]
        #print filenames
        if len(filenames) == 4:
            dbf, prj, shp, shx = [StringIO(zipfile.read(filename)) for filename in filenames]
        else:
            prj=None
            dbf, shp, shx = [StringIO(zipfile.read(filename)) for filename in filenames]
    elif re.search(r'.shp$', url):
        dir = os.path.dirname(url)
        basename = os.path.basename(url)
        basename = os.path.splitext(basename)[0]
        shp = url
        dbf, prj, shx = [os.path.join(dir, basename+"."+ext) for ext in ["dbf", "prj", "shx"]]
        if not os.path.exists(prj):
            prj=None
    elif os.path.isdir(url):
        filenames = [os.path.join(url,y) for y in sorted(os.listdir(url)) for ending in ['dbf', 'prj', 'shp', 'shx'] if y.endswith(ending)]
        if len(filenames) == 4:
            dbf, prj, shp, shx = filenames
        else:
            dbf, shp, shx = filenames
            prj = None
    else:
        dir = os.path.dirname(url)
        basename = os.path.basename(url)
        dbf, prj, shp, shx = [os.path.join(dir, basename+"."+ext) for ext in ["dbf", "prj", "shp", "shx"]]
        if not os.path.exists(prj):
            prj=None

    if type(shp) is str:
        r = shapefile.Reader(shp, shx=shx, dbf=dbf)
    else:
        r = shapefile.Reader(shp=shp, shx=shx, dbf=dbf)
    #print r.numRecords
    attributes, geometry = [], []
    field_names = [field[0] for field in r.fields[1:]]
    for row in r.shapeRecords():
        geometry.append(shape(row.shape.__geo_interface__))
        attributes.append(dict(zip(field_names, row.record)))
    #print row.shape.__geo_interface__
    if prj == None:
        warnings.warn("No projection file (.prj) found. Assuming long/lat projection.")
        proj4_string = pycrs.parser.from_epsg_code(4326)
    elif type(prj) is str:
        prj = open(prj, "r")
        prj = prj.read()
        proj4_string = pycrs.parser.from_esri_wkt(prj).to_proj4()
    else:
        proj4_string = pycrs.parser.from_esri_wkt(prj.getvalue()).to_proj4()

    #print proj4_string
    gdf = gpd.GeoDataFrame(data = attributes, geometry = geometry, crs = proj4_string)
    #print gdf.head()
    return(gdf)
